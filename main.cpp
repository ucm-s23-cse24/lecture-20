#include <iostream>
#include "board.h"
#include "tictactoe.h"
using namespace std;


Vec simpleAI(Board board){
    for (int i = 0; i < board.getSize(); i++){
        for (int j = 0; j < board.getSize(); j++){
            if (board.getGrid(i, j) == EMPTY){
                return Vec(i, j);
            }
        }
    }

    return Vec(0,0);
}

int main() {
    TicTacToe game;

    game.play();

    return 0;
}